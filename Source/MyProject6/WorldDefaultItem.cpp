// Fill out your copyright notice in the Description page of Project Settings.


#include "WorldDefaultItem.h"

// Sets default values
AWorldDefaultItem::AWorldDefaultItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AWorldDefaultItem::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWorldDefaultItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

