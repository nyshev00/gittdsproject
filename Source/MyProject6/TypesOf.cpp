// Fill out your copyright notice in the Description page of Project Settings.


#include "TypesOf.h"
#include "InterfaceGameActor.h"
#include "StateEffects.h"

void UTypes::AddEffectBySurfaceType(AActor* TakeEffectActor , FName NameBoneHit, TSubclassOf<UStateEffects> AddEffectClass, EPhysicalSurface SurfaceType)
{
	if (SurfaceType != EPhysicalSurface::SurfaceType_Default && TakeEffectActor && AddEffectClass)
	{
		UStateEffects* myEffect = Cast <UStateEffects>(AddEffectClass->GetDefaultObject());
		if (myEffect)
		{
			bool bIsHavePossibleSurface = false;
			int8 i = 0;
			while (i < myEffect->PossibleIteractSurface.Num() && !bIsHavePossibleSurface)
			{
				if (myEffect->PossibleIteractSurface[i] == SurfaceType)
				{
					bIsHavePossibleSurface = true;
					bool bIsCanAddEffect = false;
					if (!myEffect->bIsStackable)
					{
						int8 j = 0;
						TArray<UStateEffects*> CurrentEffects;
						IInterfaceGameActor* myInterface = Cast<IInterfaceGameActor>(TakeEffectActor);
						if (myInterface)
						{
							CurrentEffects = myInterface->GetAllCurrentEffects();
						}
						if (CurrentEffects.Num() > 0)
						{
							while (j < CurrentEffects.Num() && !bIsCanAddEffect)
							{
								if (CurrentEffects[j]->GetClass() != AddEffectClass)
								{
									bIsCanAddEffect = true;
								}
								j++;
							}
						}
						else
						{
							bIsCanAddEffect = true;
						}
					}
					else
					{
						bIsCanAddEffect = true;
					}
					if (bIsCanAddEffect)
					{
						UStateEffects* NewEffect = NewObject<UStateEffects>(TakeEffectActor, AddEffectClass);
						if (NewEffect)
						{
							NewEffect->InitObject(TakeEffectActor, NameBoneHit);
						}
					}
				}
				i++;
			}
		}
	}
}
