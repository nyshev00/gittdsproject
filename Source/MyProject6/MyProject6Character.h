// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TypesOf.h"
#include "WeaponDefault.h"
#include "InventoryComponent.h"
#include "InterfaceGameActor.h"
#include "StateEffects.h"
#include "MyProject6CharacterHealthComp.h"
#include "MyProject6Character.generated.h"

UCLASS(Blueprintable)
class AMyProject6Character : public ACharacter, public IInterfaceGameActor
{
	GENERATED_BODY()
protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	void InputAxisX(float Value);
	UFUNCTION()
	void InputAxisY(float Value);
	UFUNCTION()
	void InputAttackPressed();
	UFUNCTION()
	void InputAttackReleased();
	
	float AxisX = 0.0f;
	float AxisY = 0.0f;

	UDecalComponent* CurrentCursor = nullptr;

	AWeaponDefault* CurrentWeapon = nullptr;

	TArray<UStateEffects*> Effects;

	UFUNCTION()
	void CharDead();
	void EnableRagdoll();
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
	
	void TryAbilityEnabled();

public:
	AMyProject6Character();

	FTimerHandle TimerHandle_RagDollTimer;


	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComp) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory", meta = (AllowPrivateAccess = "true"))
	class UInventoryComponent* InventoryComponent;
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health", meta = (AllowPrivateAccess = "true"))
	//class UMyProject6HealthComponent* HealthComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health", meta = (AllowPrivateAccess = "true"))
	class UMyProject6CharacterHealthComp* CharHealthComponent;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementInfo;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool SprintRunEnabled = false;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool WalkEnabled = false;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool AimEnabled = false;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool bIsAlive = true;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool bIsStunned = false;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
		TSubclassOf<UStateEffects> AbilityEffect;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		TArray<UAnimMontage*> DeadAnim;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stunning")
		TArray<UAnimMontage*> StunAnim;

	//UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Weapon")
	//	FName InitWeaponName;
	//UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Weapon")
	//	FAdditionalWeaponInfo WeaponAdditionalInfo;

	bool IsSprinting = false;
	
	UFUNCTION()
	void MovementTick(float DeltaTime);

	UFUNCTION(BlueprintCallable)
		void AttackCharEvent(bool bIsFiring);
	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementState(EMovementState NewMovementState);

	
	//for demo 
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo");
	//	TSubclassOf<AWeaponDefault> InitWeaponClass = nullptr;
 
	UFUNCTION(BlueprintCallable)
		AWeaponDefault* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable)
		void InitWeapon( FName IdWeaponName, FAdditionalWeaponInfo WeaponInfo, int32 NewCurrentIndexWeapon);
	UFUNCTION(BlueprintCallable)//VisualOnly
		void RemoveCurrentWeapon();
	UFUNCTION(BlueprintCallable)
		void TryReloadWeapon();
	UFUNCTION()
		void WeaponReloadStart(UAnimMontage* Anim);
	UFUNCTION()
		void WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe);
	
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadStart_BP(UAnimMontage* Anim);
	//UFUNCTION(BlueprintNativeEvent)
	//	void WeaponReloadEnd_BP();
	UFUNCTION()
		void WeaponFireStart(UAnimMontage* Anim);
	//UFUNCTION(BlueprintNativeEvent)
	//	void WeaponFireStart_BP(UAnimMontage* Anim);

	UFUNCTION(BlueprintCallable)
		UDecalComponent* GetCursorToWorld();

	void TrySwitchNextWeapon();
	void TrySwitchPreviosWeapon();

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
		int32 CurrentIndexWeapon = 0;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 IndexForSwitch = 0;
		//if you want to call function from InterfaceGameActor - use functionname_Implementation() override
		EPhysicalSurface GetSurfaceType() override;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool GetIsAlive();
	
	TArray<UStateEffects*> GetAllCurrentEffects() override;
	void RemoveEffect(UStateEffects* RemoveEffect) override;
	void AddEffect(UStateEffects* newEffect) override;


	void CharStunned();
	void CharUnStunned();

	FVector GettingLocation();

	UFUNCTION(BlueprintNativeEvent)
	void CharDead_BP();
};

