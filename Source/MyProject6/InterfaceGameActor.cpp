// Fill out your copyright notice in the Description page of Project Settings.


#include "InterfaceGameActor.h"

// Add default functionality here for any IInterfaceGameActor functions that are not pure virtual.

EPhysicalSurface IInterfaceGameActor::GetSurfaceType()
{
	return EPhysicalSurface::SurfaceType_Default;
}

TArray<UStateEffects*> IInterfaceGameActor::GetAllCurrentEffects()
{
	TArray<UStateEffects*> Effect;
	return Effect;
}

void IInterfaceGameActor::RemoveEffect(UStateEffects* RemoveEffect)
{
}

void IInterfaceGameActor::AddEffect(UStateEffects* newEffect)
{
}
