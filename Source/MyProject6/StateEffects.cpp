// Fill out your copyright notice in the Description page of Project Settings.


#include "StateEffects.h"
#include "MyProject6HealthComponent.h"
#include "InterfaceGameActor.h"
#include "MyProject6Character.h"
#include "Kismet/GameplayStatics.h"

bool UStateEffects::InitObject(AActor* Actor, FName NameBoneHit)
{
	myActor = Actor;
	return true;
}

void UStateEffects::DestroyObject()
{
	IInterfaceGameActor* myInterface = Cast<IInterfaceGameActor>(myActor);
	if (myInterface)
	{
		myInterface->RemoveEffect(this);
	}
	myActor = nullptr;
	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
}

bool UStateEffects_ExecuteOnce::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);
	ExecuteOnce();
	return true;
}

void UStateEffects_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UStateEffects_ExecuteOnce::ExecuteOnce()
{
	UE_LOG(LogTemp, Warning, TEXT("ExecuteOnce"));

	if (myActor)
	{
		UMyProject6HealthComponent* myHealthComp = Cast<UMyProject6HealthComponent>(myActor->GetComponentByClass(UMyProject6HealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue(Power);
		}
	}
	DestroyObject();
}

bool UStateEffects_ExecuteTimer::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);

	GetWorld()->GetTimerManager().SetTimer(EffectTimer, this, &UStateEffects_ExecuteTimer::DestroyObject, Timer, false);
	GetWorld()->GetTimerManager().SetTimer(ExecuteTimer, this, &UStateEffects_ExecuteTimer::Execute, RateTime, true);

	if (ParticleEffect)
	{
		FName NameBoneToAttach = NameBoneHit;
		FVector Location = FVector(0);
		USceneComponent* myMesh = Cast<USceneComponent> (myActor->GetComponentByClass(USkeletalMesh::StaticClass()));
		if (myMesh)
		{
			ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myMesh, NameBoneToAttach, Location, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		}
		else
		{
			ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(), NameBoneToAttach, Location, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		}
	}
	return true;
}

void UStateEffects_ExecuteTimer::DestroyObject()
{
	ParticleEmitter->DestroyComponent();
	ParticleEmitter = nullptr;
	Super::DestroyObject();
}

void UStateEffects_ExecuteTimer::Execute()
{
	if (myActor)
	{
		UMyProject6HealthComponent* myHealthComp = Cast<UMyProject6HealthComponent>(myActor->GetComponentByClass(UMyProject6HealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue(Power);
		}
	}
}

bool UStateEffects_Stunning::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);
	Stunning();
	return true;
}

void UStateEffects_Stunning::DestroyObject()
{
	AMyProject6Character* myChar = Cast<AMyProject6Character>(myActor);
	myChar->CharUnStunned();
	Super::DestroyObject();
}

void UStateEffects_Stunning::Stunning()
{
	AMyProject6Character* myChar = Cast<AMyProject6Character>(myActor);
	myChar->CharStunned();
	GetWorld()->GetTimerManager().SetTimer(StunTimer, this, &UStateEffects_Stunning::DestroyObject, Timer, false);
}

bool UStateEffects_Superpower::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);

	GetWorld()->GetTimerManager().SetTimer(EffectTimer, this, &UStateEffects_Superpower::DestroyObject, Timer, false);
	GetWorld()->GetTimerManager().SetTimer(SuperpowerTimer, this, &UStateEffects_Superpower::Superpower, RateTime, false);

	if (ParticleEffect)
	{
		FName NameBoneToAttach;
		FVector Location = FVector(0);
		ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(), NameBoneToAttach, Location, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
	}
	return true;
}

void UStateEffects_Superpower::DestroyObject()
{
	if (ParticleEmitter)
	{
		ParticleEmitter->DestroyComponent();
		ParticleEmitter = nullptr;
	}
	UMyProject6CharacterHealthComp* myHealth = Cast<UMyProject6CharacterHealthComp>(myActor->GetComponentByClass(UMyProject6HealthComponent::StaticClass()));
	if (myHealth)
	{
		myHealth->CanHealthChanged(true);
		myHealth->GetOwner()->SetCanBeDamaged(true);
	}
	Super::DestroyObject();
}

void UStateEffects_Superpower::Superpower()
{
	UMyProject6CharacterHealthComp* myHealth = Cast<UMyProject6CharacterHealthComp>(myActor->GetComponentByClass(UMyProject6HealthComponent::StaticClass()));
	
	if (myHealth)
	{
		myHealth->CanHealthChanged(false);
		myHealth->GetOwner()->SetCanBeDamaged(false);
	}
}
