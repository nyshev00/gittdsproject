// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MyProject6HealthComponent.h"
#include "MyProject6CharacterHealthComp.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Shield, float, Damage);

UCLASS()
class MYPROJECT6_API UMyProject6CharacterHealthComp : public UMyProject6HealthComponent
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Shield")
	FOnShieldChange OnShieldChange;

	FTimerHandle CooldownShield;
	FTimerHandle ShieldRecoveryRateTimer;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
	float CooldownShieldRecoveryTime = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
	float ShieldRecoveryValue = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
	float ShieldRecoveryRate = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
	bool bIsCanChange = true;
protected:
	float Shield = 100.0f;
public:

	void ChangeHealthValue(float ChangeValue) override;

	float GetCurrentShield();

	void ChangeShieldValue(float ChangeValue);

	void CooldownShieldEnd();

	void RecoveryShield();
	
	bool CanHealthChanged(bool bCanChange);
	UFUNCTION(BlueprintCallable)
	float GetShieldValue();
};
