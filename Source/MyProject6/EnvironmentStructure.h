// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InterfaceGameActor.h"
#include "StateEffects.h"
#include "EnvironmentStructure.generated.h"

UCLASS()
class MYPROJECT6_API AEnvironmentStructure : public AActor, public IInterfaceGameActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEnvironmentStructure();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	EPhysicalSurface GetSurfaceType() override;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
	TArray<UStateEffects*> Effects;

	TArray<UStateEffects*> GetAllCurrentEffects() override;
	void RemoveEffect(UStateEffects* RemoveEffect) override;
	void AddEffect(UStateEffects* newEffect) override;
};
