// Fill out your copyright notice in the Description page of Project Settings.


#include "MyProject6CharacterHealthComp.h"



void UMyProject6CharacterHealthComp::ChangeHealthValue(float ChangeValue)
{
	float CurrentDamage = ChangeValue * CoefDamage;
	if (Shield > 0.0f && ChangeValue < 0.0f)
	{
		ChangeShieldValue(ChangeValue);
		if (Shield < 0.0f)
		{
			//FX
		}
	}
	else
	{
		if (bIsCanChange)
		{
			Super::ChangeHealthValue(ChangeValue);
		}
	}
}

float UMyProject6CharacterHealthComp::GetCurrentShield()
{
	return Shield;
}

void UMyProject6CharacterHealthComp::ChangeShieldValue(float ChangeValue)
{
	Shield += ChangeValue;

	OnShieldChange.Broadcast(Shield, ChangeValue);

	if (Shield > 100.0f)
	{
		Shield = 100.0f;
	}
	else
	{
		if (Shield < 0.0f)
		{
			Shield = 0.0f;
		}
	}
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(CooldownShield, this, &UMyProject6CharacterHealthComp::CooldownShieldEnd, CooldownShieldRecoveryTime, false);

		GetWorld()->GetTimerManager().ClearTimer(ShieldRecoveryRateTimer);
	}
}

void UMyProject6CharacterHealthComp::CooldownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(ShieldRecoveryRateTimer, this, &UMyProject6CharacterHealthComp::RecoveryShield, ShieldRecoveryRate, true);
	}
}

void UMyProject6CharacterHealthComp::RecoveryShield()
{
	float tmp = Shield;
	tmp = tmp + ShieldRecoveryValue;
	if (tmp > 100.0f)
	{
		Shield = 100.0f;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(ShieldRecoveryRateTimer);
		}
	}
	else
	{
		Shield = tmp;
	}
	OnShieldChange.Broadcast(Shield, ShieldRecoveryValue);
}

bool UMyProject6CharacterHealthComp::CanHealthChanged(bool bCanChange)
{
	bIsCanChange = bCanChange;
	return bCanChange;
}

float UMyProject6CharacterHealthComp::GetShieldValue()
{
	return Shield;
}
