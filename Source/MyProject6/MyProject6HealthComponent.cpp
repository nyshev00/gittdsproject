#include "MyProject6HealthComponent.h"
#include "Net/UnrealNetwork.h"

UMyProject6HealthComponent::UMyProject6HealthComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

}

void UMyProject6HealthComponent::BeginPlay()
{
	Super::BeginPlay();
}


// Called every frame
void UMyProject6HealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

float UMyProject6HealthComponent::GetCurrentHealth()
{
	return Health;
}

void UMyProject6HealthComponent::SetCurrentHealth(float NewHealth)
{
	Health = NewHealth;
}

bool UMyProject6HealthComponent::GetIsAlive()
{
	return bIsAlive;
}

void UMyProject6HealthComponent::ChangeHealthValue_Implementation(float ChangeValue)
{
	if (bIsAlive)
	{
		ChangeValue = ChangeValue * CoefDamage;
		Health += ChangeValue;

		OnHealthChange.Broadcast(Health, ChangeValue);

		if (Health > 100.0f)
		{
			Health = 100.0f;
		}
		else
		{
			if (Health <= 0.0f)
			{
				bIsAlive = false;
				OnDead.Broadcast();
			}
		}
	}
}
void UMyProject6HealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UMyProject6HealthComponent, Health);
	DOREPLIFETIME(UMyProject6HealthComponent, bIsAlive);

}
