// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Particles/ParticleSystemComponent.h"
#include "StateEffects.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class MYPROJECT6_API UStateEffects : public UObject
{
	GENERATED_BODY()
public:
	virtual bool InitObject(AActor* Actor, FName NameBoneHit);

	virtual void DestroyObject();

	UPROPERTY(EditAnywhere,BlueprintReadWrite, Category = "Settings")
	TArray<TEnumAsByte<EPhysicalSurface>> PossibleIteractSurface;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
	bool bIsStackable = false;


	AActor* myActor = nullptr;
};

UCLASS()
class MYPROJECT6_API UStateEffects_ExecuteOnce : public UStateEffects
{
	GENERATED_BODY()

public:
	bool InitObject(AActor* Actor, FName NameBoneHit) override;
	void DestroyObject() override;

	virtual void ExecuteOnce();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Once")
	float Power = 20.0f;
};

UCLASS()
class MYPROJECT6_API UStateEffects_ExecuteTimer : public UStateEffects
{
	GENERATED_BODY()

public:
	bool InitObject(AActor* Actor, FName NameBoneHit) override;
	void DestroyObject() override;

	virtual void Execute();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Timer")
	float Power = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Timer")
	float Timer = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Timer")
	float RateTime = 1.0f;

	FTimerHandle ExecuteTimer;
	FTimerHandle EffectTimer;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Timer")
	UParticleSystem* ParticleEffect = nullptr;

	UParticleSystemComponent* ParticleEmitter = nullptr;
};

UCLASS()
class MYPROJECT6_API UStateEffects_Stunning : public UStateEffects
{
	GENERATED_BODY()

public:
	bool InitObject(AActor* Actor, FName NameBoneHit) override;
	void DestroyObject() override;

	FTimerHandle StunTimer;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Stunning")
	float Timer = 3.0f;

	virtual void Stunning();
};

UCLASS()
class MYPROJECT6_API UStateEffects_Superpower :public UStateEffects
{
	GENERATED_BODY()
	
public:
	bool InitObject(AActor* Actor, FName NameBoneHit) override;
	void DestroyObject() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Superpower")
	float Timer = 10.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Superpower")
	float RateTime = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Superpower")
	UParticleSystem* ParticleEffect = nullptr;

	UParticleSystemComponent* ParticleEmitter = nullptr;
	FTimerHandle EffectTimer;
	FTimerHandle SuperpowerTimer;

	virtual void Superpower();
};