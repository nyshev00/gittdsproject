// Copyright Epic Games, Inc. All Rights Reserved.

#include "MyProject6Character.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
//#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "InventoryComponent.h"
#include "MyProject6GameInstance.h"
#include "WeaponDefault.h"
#include "MyProject6CharacterHealthComp.h"
#include "ProjectileDefault.h"
#include "StateEffects.h"
#include "MyProject6PlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"

AMyProject6Character::AMyProject6Character()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	InventoryComponent = CreateDefaultSubobject<UInventoryComponent>(TEXT("InventoryComponent"));
	CharHealthComponent = CreateDefaultSubobject<UMyProject6CharacterHealthComp>(TEXT("HealthComponent"));
	if (CharHealthComponent)
	{
		CharHealthComponent->OnDead.AddDynamic(this, &AMyProject6Character::CharDead);
	}
	if (InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &AMyProject6Character::InitWeapon);
	}
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void AMyProject6Character::BeginPlay()
{
	Super::BeginPlay();

	if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}
}

void AMyProject6Character::Tick(float DeltaSeconds)
{
	InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->WeaponInfo);
    Super::Tick(DeltaSeconds);

	if (CurrentCursor)
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if (myPC)
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}
	MovementTick(DeltaSeconds);
}

void AMyProject6Character::SetupPlayerInputComponent(UInputComponent* InputComp)
{
	Super::SetupPlayerInputComponent(InputComp);

	InputComp->BindAxis(TEXT("MoveForward"), this, &AMyProject6Character::InputAxisX);
	InputComp->BindAxis(TEXT("MoveRight"), this, &AMyProject6Character::InputAxisY);

	InputComp->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &AMyProject6Character::InputAttackPressed);
	InputComp->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &AMyProject6Character::InputAttackReleased);
	InputComp->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &AMyProject6Character::TryReloadWeapon);

	InputComp->BindAction(TEXT("SwitchNextWeapon"), EInputEvent::IE_Pressed, this, &AMyProject6Character::TrySwitchNextWeapon);
	InputComp->BindAction(TEXT("SwitchPreviosWeapon"), EInputEvent::IE_Pressed, this, &AMyProject6Character::TrySwitchPreviosWeapon);

	InputComp->BindAction(TEXT("Abitity"), EInputEvent::IE_Pressed, this, &AMyProject6Character::TryAbilityEnabled);
}

void AMyProject6Character::InputAxisX(float Value)
{
	AxisX = Value;
}

void AMyProject6Character::InputAxisY(float Value)
{
	AxisY = Value;
}

void AMyProject6Character::InputAttackPressed()
{
	if (bIsAlive)
	{
		AttackCharEvent(true); 
	}
}

void AMyProject6Character::InputAttackReleased()
{
	AttackCharEvent(false);
}

void AMyProject6Character::MovementTick(float DeltaTime)
{
	if (bIsAlive && !bIsStunned)
	{
		AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
		AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

		if (MovementState == EMovementState::Sprint_State)
		{
			IsSprinting = true;
			FVector myRotationVector = FVector(AxisX, AxisY, 0.0f);
			FRotator myRotator = myRotationVector.ToOrientationRotator();
			SetActorRotation((FQuat(myRotator)));
		}
		else
		{
			IsSprinting = false;
			APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
			if (myController)
			{
				FHitResult ResultHit;
				myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);

				float FindRotaterResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
				SetActorRotation(FQuat(FRotator(0.0f, FindRotaterResultYaw, 0.0f)));

				if (CurrentWeapon)
				{
					FVector Displacement = FVector(0);
					switch (MovementState)
					{
					case EMovementState::Aim_State:
						Displacement = FVector(0.0f, 0.0f, 0.0f);
						CurrentWeapon->ShouldReduceDispersion = true;
						break;
					case EMovementState::Walk_State:
						Displacement = FVector(0.0f, 0.0f, 120.0f);
						CurrentWeapon->ShouldReduceDispersion = true;
						break;
					case EMovementState::Run_State:
						Displacement = FVector(0.0f, 0.0f, 120.0f);
						CurrentWeapon->ShouldReduceDispersion = true;
						break;
					default:
						break;
					}
					CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacement;
				}

			}
		}
		if (CurrentWeapon)
		{
			if (FMath::IsNearlyZero(GetVelocity().Size(), 0.5f))
				CurrentWeapon->ShouldReduceDispersion = true;
			else
				CurrentWeapon->ShouldReduceDispersion = false;
		}
	}
}

void AMyProject6Character::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (!IsSprinting && !bIsStunned)
	{
		if (myWeapon)
		{
			myWeapon->SetWeaponStateFire(bIsFiring);
		}
	}
}

void AMyProject6Character::ChangeMovementState(EMovementState NewMovementState)
{
	if (!bIsStunned)
	{
		MovementState = NewMovementState;
		CharacterUpdate();

		AWeaponDefault* myWeapon = GetCurrentWeapon();
		if (myWeapon)
		{
			myWeapon->UpdateStateWeapon(NewMovementState);
		}
	}
}

void AMyProject6Character::CharacterUpdate()
{
	float ResSpeed = 800.0f;
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeed;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.RunSpeed;
		break;
	case EMovementState::Sprint_State:
		ResSpeed = MovementInfo.SprintSpeed;
		break;
	default:
		break;
	}
GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

AWeaponDefault* AMyProject6Character::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void AMyProject6Character::InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponInfo, int32 NewCurrentIndexWeapon)
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	UMyProject6GameInstance* myGI = Cast<UMyProject6GameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocket"));
					CurrentWeapon = myWeapon;

					myWeapon->IdWeaponName = IdWeaponName;
					myWeapon->WeaponSetting = myWeaponInfo;
					//myWeapon->WeaponInfo.Round = myWeaponInfo.MaxRound;
					
					myWeapon->ReloadTime = myWeaponInfo.ReloadTime;
					myWeapon->UpdateStateWeapon(MovementState);

					myWeapon->WeaponInfo = WeaponInfo;

					CurrentIndexWeapon = NewCurrentIndexWeapon;

					myWeapon->OnWeaponReloadStart.AddDynamic(this, &AMyProject6Character::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &AMyProject6Character::WeaponReloadEnd);
					myWeapon->OnWeaponFireStart.AddDynamic(this, &AMyProject6Character::WeaponFireStart);

					if (InventoryComponent)
					{
					//	CurrentIndexWeapon = InventoryComponent->GetWeaponIndexSlotByName(IdWeaponName);
					}
					if (CurrentWeapon->GetWeaponRound() == 0 && CurrentWeapon->CanWeaponReload())
					{
						CurrentWeapon->InitReload();
					}
				}
			}
		}
	}
}

void AMyProject6Character::RemoveCurrentWeapon()
{
}

void AMyProject6Character::TryReloadWeapon()
{
	if (bIsAlive && CurrentWeapon)
	{
		if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound && CurrentWeapon->CanWeaponReload())
		{
			CurrentWeapon->InitReload();
		}
	}
}

void AMyProject6Character::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
}

void AMyProject6Character::WeaponReloadEnd(bool bIsSuccess, int32 AmmoNeedTake)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->WeaponChangeAmmo(CurrentWeapon->WeaponSetting.WeaponType, -AmmoNeedTake);
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->WeaponInfo);
	}
}

void AMyProject6Character::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
}

void AMyProject6Character::WeaponFireStart(UAnimMontage* Anim)
{
	InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->WeaponInfo);

}

UDecalComponent* AMyProject6Character::GetCursorToWorld()
{
	return CurrentCursor;
}

void AMyProject6Character::TrySwitchPreviosWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1 && !bIsStunned && !CurrentWeapon->WeaponReloading)
	{
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;
			UE_LOG(LogTemp, Display, TEXT("Old info round = , %d"), OldInfo.Round);

			if (CurrentWeapon->WeaponReloading)
			{
				CurrentWeapon->CancelReload();
			}
		}
		if (InventoryComponent)
		{
		//	InventoryComponent->SetAdditionalInfoWeapon(OldIndex, GetCurrentWeapon()->WeaponInfo);
			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon - 1, OldIndex, OldInfo, false))
			{
			}
		}
	}
}

float AMyProject6Character::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	if (bIsAlive)
	{
		CharHealthComponent->ChangeHealthValue(-DamageAmount);
	}

	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		AProjectileDefault* myProjectile = Cast <AProjectileDefault>(DamageCauser);
		if (myProjectile)
		{
			UTypes::AddEffectBySurfaceType(this,NAME_None, myProjectile->ProjectileSetting.Effect, GetSurfaceType());
		}
	}
	return ActualDamage;
}

void AMyProject6Character::CharDead_BP_Implementation()
{
	//BP
}

void AMyProject6Character::CharDead()
{
	float TimeAnim = 3.0f;
	int32 rnd = FMath::RandHelper(DeadAnim.Num());
	if (DeadAnim.IsValidIndex(rnd) && DeadAnim[rnd] && GetMesh() && GetMesh()->GetAnimInstance())
	{
		DeadAnim[rnd]->GetPlayLength();
		GetMesh()->GetAnimInstance()->Montage_Play(DeadAnim[rnd]);
	}
	bIsAlive = false;

	if (GetController())
	{
		GetController()->UnPossess();
	}
	UnPossessed();

	GetWorldTimerManager().SetTimer(TimerHandle_RagDollTimer, this, &AMyProject6Character::EnableRagdoll, TimeAnim, false);

	//GetCursorToWorld()->SetVisibility(false);

	AttackCharEvent(false);

	CharDead_BP();
}

void AMyProject6Character::CharStunned()
{
	int32 rnd = FMath::RandHelper(StunAnim.Num());
	if (StunAnim.IsValidIndex(rnd) && StunAnim[rnd] && GetMesh() && GetMesh()->GetAnimInstance())
	{
		GetMesh()->GetAnimInstance()->Montage_Play(StunAnim[rnd]);
		bIsStunned = true;
	}
}

void AMyProject6Character::CharUnStunned()
{
	APlayerController* myPC = Cast<APlayerController>(GetController());
	bIsStunned = false;
}

FVector AMyProject6Character::GettingLocation()
{
	return GetActorLocation();
}

EPhysicalSurface AMyProject6Character::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	if (CharHealthComponent)
	{
		if (CharHealthComponent->GetCurrentShield() <= 0)
		{
			if (GetMesh())
			{
				UMaterialInterface* myMaterial = GetMesh()->GetMaterial(0);
				if (myMaterial)
				{
					Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
				}
			}
		}
	}
	return Result;
}

bool AMyProject6Character::GetIsAlive()
{
	return bIsAlive;
}

void AMyProject6Character::EnableRagdoll()
{
	if (GetMesh())
	{
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
}

TArray<UStateEffects*> AMyProject6Character::GetAllCurrentEffects()
{
	return Effects;
}

void AMyProject6Character::RemoveEffect(UStateEffects* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}

void AMyProject6Character::AddEffect(UStateEffects* newEffect)
{
	Effects.Add(newEffect);
}

void AMyProject6Character::TryAbilityEnabled()
{
	if (AbilityEffect)
	{
		UStateEffects* NewEffect = NewObject<UStateEffects>(this, AbilityEffect);
		if (NewEffect)
		{
			NewEffect->InitObject(this, NAME_None);
		}
	}
}


void AMyProject6Character::TrySwitchNextWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1 && !bIsStunned && !CurrentWeapon->WeaponReloading)
	{
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;
			UE_LOG(LogTemp, Display, TEXT("Old info round = , %d"), OldInfo.Round);

			if (CurrentWeapon->WeaponReloading)
			{
				CurrentWeapon->CancelReload();
			}
		}
		if (InventoryComponent)
		{
	//		InventoryComponent->SetAdditionalInfoWeapon(OldIndex, GetCurrentWeapon()->WeaponInfo);
			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo, true))
			{
			}
		}
	}
}
